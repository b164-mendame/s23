





Activity:

//3. Insert a single room (insertOne method) with the following details:

db.users.insert(
   {
	name : "single",
    accomodates : 2
    price : 1000,
    description: "A simple room with all the basic necessities"
    rooms_available: 10
    isAvailable : false
}
)




//4. Insert multiple rooms (insertMany method) with the following details:


db.users.insertMany([
    {
	name : "Double Room",
    accomodates : 3
    price : 2000,
    description: "A room fit for a small family going on a vacation"
    rooms_available: 5
    isAvailable : false
},
{
	name : "Queen Room",
    accomodates : 4
    price : 4000,
    description: "A room with a queen sized bed perfect for a simple getaway"
    rooms_available: 15
    isAvailable : false
},
	
    
 ])


//5. Use the find method to search for a room with the name double.

db.users.find({
    
    name: "Double Room"
    
    })
    

//6. Use the updateOne method to update the queen room and set the available rooms to 0.


db.users.updateOne(
		    { name: "Queen Room" },
		    {
		        $set : {
		          
		          rooms_available: 15
    
		        }
		    }
		);


//7. Use the deleteMany method rooms to delete all rooms that have 0 availability.

db.users.deleteOne(

{ rooms_available: 0 }

)
